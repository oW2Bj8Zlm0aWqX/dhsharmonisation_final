\documentclass[10pt,english]{article}
\usepackage[english]{babel}
\usepackage{longtable}
\usepackage{fontspec}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{varioref} %% vref
\usepackage{paralist} %% compactenum
\begin{document}

\section{Introduction}
\label{sec:introduction}

Demographic and Health Surveys (DHS) is a household survey program focussing on living conditions and reproductive health for low and middle income countries run by USAid since 1985. Over 90 countries are covered and in total there are currently (november 2019) 404 surveys available and 34 surveys are ongoing. The DHS surveys aim to create valid statistics at the population level for separate countries, but for the researcher interested in the global situation, and for comparative studies DHS offers a fantastic source of data if only the data is harmonised. The devil is in the detail, the saying goes, and in this case it applies to the subtle but important ways in which the surveys differ. Standardising such differences becomes a major task for researchers wanting to use data from many countries in their research (ref to Ekbrand \& Halleröd). This article will present the R package ``globallivingconditions'' which offers an automated way for researchers to get access to harmonised DHS data on living conditions. (Reproductive health is not yet covered by the package, but the as such the framework lends itself well for extensions in that regard).

The DHS data is provided only by the DHS-website and access requires registration, where the researcher must state the purpose of the data aquisition. The researcher is not allowed to share the data provided by the DHS. The results of harmonising efforts of the DHS data cannot be shared between researchers, since that would violate the agreement between the researcher and DHS. What can be freely shared though, is a program that performs the harmonisation, and the R package ``globallivingconditions'' is such a program.

\section{Aim}
The article help researchers interested in using DHS on many countries to understand what the R package ``globallivingconditions'' offers for them and how they can use it to fast get a big harmonised dataset. Limitations of the package and caveats are lined out.

\section{Features}

The package offers the following features:

\begin{itemize}
\item Automatic downloading of data from the DHS website.
\item Automatic and transparent recoding of categorical variables which joins labels that differ only linguistically.
\item Automatic and transparent recoding of numerical variables that takes into account meta-data about what the recorded values represent.
\item Derivation of a number synthetical variables pertaining to living conditions, including child deprivation indicators and the international wealth index.
\item Options to automatically add variables from external sources: including country level data from the Quality of government institute and geocoded data on temperature, precipitation, malaria.
\end{itemize}

In this context, transparency means ``with the ability to account for all transformations made to the original data''.


\section{Previous work}
\label{sec:previous-work}
DHS has long been for research, most often in single country studies, but in comparative studies as well. David Gordon and Shailen Nandy harmonised a large set of DHS-surveys for their estimations of global child poverty. The technique they used was to start with a template script which was then adapted for every new survey, so in the end they had the same number of scripts as they had number of surveys to harmonize. The problem with that technique is that it does not build knowledge useful to take on future surveys, so whenever there is a new survey released by DHS, the same amount of work is needed to harmonise it as the survey before that one.

\section{Method}
\label{sec:method}

While the user of the package does not need to understand how the package accomplishes what it accomplishes, users that would like to change the package for their specific needs, or contribute to the package benefit from this section.

\subsection{Recoding variable values}
\label{sec:recod-vari-valu}

The innovate idea at the heart of this package is that most tasks in harmonisation can be carried out in a way that improves the probability that newer surveys will ``just work''. A central problem in data harmonisation is that the set of recorded values on a categorical variable is much larger than needed, due to purely lingvistic variations of the same underlying concept. For example, consider the various values on the variable ``Relation to the head of household'' that represent the same thing, namely 'Adopted/foster/stepchild':

\begin{tabular}{l}
'Stepchild' \\
'Adopted/ foster/ stepchild' \\
'Adopted/foster/step child' \\
'Adopted/foster/stepchild' \\
'Adopted/foster child/stepchild' \\
'Adopted/foster child' \\
'Adopted/foster child/step child' \\
'Step child' \\
'Son/daughter of wife or husband {CG}'  \\
'Step-son/step-daughter' \\
'Stepson/daughter' \\
'Adopted child' \\
'Adopted/foster child/child of wife / husband' \\
\end{tabular}



A researcher that works by adapting a template to a specific country would have to identify the label corresponding to the underlying concept and make sure that the template does the right recoding or change the recoding. This task must repeated for every new survey. By dealing with all countries at the same time, and programmatically extract a list of all existing values on a categorical variable, it is fairly easy to create the list of all labels that should be recoded to the same target value - and the more countries already process, the more likely this list will cover the variations present in \emph{future} surveys as well. 

Harmonising values that only differ linguistically can be done in different ways, and this package relies on two techniqes for that. Firstly, the method described above, programmatically extract all existing value labels, manually sort the value labels into different labeled sets (the label is target value). Secondly, build a suitable regular expression to categorise value labels. The former is easier and comes without risk of unanticipiated consequences, the latter is more difficult, but it has the advantage that it is not dependent on the exact linguistic expression, so if new surveys comes with previously unseen values these can be dealt with, the results needs to be checked though. In short, the regular expression approach provides the researcher a suggested categorisation of new values, while manual sorting leaves new values untouched, so the researcher has to sort them manually.

Since the package is implemented in R, which has good support for machine learning frameworks, machine learning techniques could be incorporated in the future, and there is an embryo for classification of occupations that use machine learning techniques.

\subsection{Implementation}
\label{sec:implementation}

The package exports a single command, \texttt{download.and.harmonise} which returns a harmonised dataset.

The package is implemented in R using the literate programming framework knitr to weave documentation and code. The implementation is broken down into (relatively) small chunks to enchance readability of the code. Major efforts have gone into keeping the memory requirements low while still allowing for parallel processing and efficient release of free RAM. To accomplish these goals the package makes heavy use of snow, data.table, foreach, doParallel and future. For deriving the superclusters a range of GIS related packages are used: sp, rgdal, spdep, geosphere, rgeos, deldir, SearchTrees. All in all, the dependencies of the packages are substantial, and for usage on a Operating System that compiles R packages from source there are some external dependencies. For users of GNU/Linux, the following packages (or later versions thereof) must be installed prior to installing ``globallivingconditions'':

libgdal-dev
libproj-dev
libgdal20
libxml2-dev
libudunits2-dev
libcurl4-gnutls-dev
libssl-dev

On Debian based distributions, including e.g. Ubuntu, the following command installs them:

<<engine="bash", eval=FALSE>>=
sudo apt-get install libgdal-dev libproj-dev libgdal20 libxml2-dev libudunits2-dev libcurl4-gnutls-dev libssl-dev
@ 

The latest two surveys from India provide a special challenge in that the sheer size of these datasets make the memory requirements to import them via \texttt{foreign::read.spss()} are quite massive, about 40 GB of RAM. For users with less RAM than 40 GB, either exclude those two surveys from the dataset (see below on how to do that), or temporarily use another computer, e.g. by temporary renting a high end computer from Google Cloud Platform (GCP) or Amazon Web Services (AWS). Helper scripts for setting up an instance on GCP is provided as supplementary material to this article. The costs for running an GCP instance to harmonize the complete DHS archive is only \$3, but requires registration on GCP.




since the complete DHS archive contains about 23 GB of data. On a computer with 8 GB of RAM, 

\section{Results}
\label{sec:results}

This sections details the outcome returned by the package.

\subsection{Transparency}
\label{sec:transparency}

All recoding of values to a harmonised label is explicit and easy to check. A report of all recodes is automatically created, the researcher can thus check that the recodings are sensible, and are kindly asked to report questionable recodings as bugs of the package.

\section{Derived variables}
\label{sec:derived-variables}

In addition to harmonisation, the package provides a set of derived variables, most of which are aiming to describe different aspects of deprivation and in particular deprivation in children, but also some technical variables useful for longitudinal analysis.

\subsection{Variables about deprivations}
\label{sec:vari-about-depr}

The package use a definition of child deprivation based on international agreements, specifically the 1995 \emph{Copenhagen declaration and programme of action}, which states:

\begin{quote}
Absolute poverty is a condition characterized by severe deprivation of basic human needs, including food, safe drinking water, sanitation facilities, health, shelter, education and information. (United Nations, 1995).
\end{quote}

Even if there is a widely accepted agreement on which basic human needs that must be fulfilled for a child not be deprived, the thresholds which separate the non-deprived from the deprived still need to be operationalised, a work that has been led by Gordon in what have been known as `the Bristol Approach' (Gordon \& Nandy 2012; UNICEF 2011). Deprivation have been conceptualised by Gordon \emph{et al.} as a continuum between no deprivation and extreme deprivation, with `mild', `moderate' and `severe' deprivation lying between the ends of the continuum. Operationalisations have been constructed for `mild', `moderate' and `severe' deprivations, and (Gordon \emph{et al.} 2003, p. 8), and to err on the side of caution the authors used the measures for `severe' deprivations. The principle used to define the thresholds for the `severe' deprivations was that the severe deprivations should be causally linked to severe adverse long and short term effects on health, well-being and development of children (Gordon \& Nandy 2012, p 59). Subsequent works that use the Bristol approach have continued to rely on the measures for `severe' deprivation (e.g Nandy 2009; Halleröd \emph{et al.} 2013). The following operationalisations constitute `severe' deprivation in each of the seven areas defined by the Copenhagen declaration. (NB. Most measures are only defined for specific age groups, e.g. educational deprivation is only defined for children above 6 years of age).

  \begin{itemize}
  \item Safe Water: Children who only have access to surface water (for example, rivers) for drinking or who lived in households where the nearest source of water was more than 15 min away. Children <18 years old
  \item Food: Children whose heights and weights for their age were more than -3 standard deviations below the median of the international reference, that is, severe anthropometric failure. Children <5 years old
  \item Sanitation: Children who had no access to a toilet of any kind in the vicinity of their dwelling, that is, no private or communal toilets or latrines. Children <18 years old
  \item Health: Children who had not been immunized against diseases or young children who had a recent illness involving diarrhea and had not received medical advice or treatment. Children <5 years old
  \item Shelter: Children in dwellings with more than five people per room and/or with no flooring material. Children <18 years old
  \item Education: Children who had never been to school and were not currently attending school, i.e., no professional education of any kind. Children 7–17 years old
  \item Information: Children with no access to radio, television, telephone, or newspaper at home. Children 3–17 years old
\end{itemize}

\subsection{The International Wealth Index}
\label{sec:intern-wealth-index}

DHS provides a wealth index, but this index is only anchored nationally, to get a globally anchored index, another approach is needed and Global data lab, at Nijmegen Center for Economics(NiCE), Institute for Management Research has defined The International Wealth Index as a weighted score on a set of items from DHS for this purpose. The package implements a calculation of this index, based on a SPSS-script by Jeroen Smits dated 20 september 2014.

\subsection{Variables for statistical analysis}
\label{sec:vari-stat-analys}

To ease longitudinal analysis of the repeated cross-sectional data provided by the DHS, the package harmonises the region names and derives a new variable called super cluster, which defines a geographic partition of each country so that each polygon includes a measurement point from each survey.

The super cluster makes within-country comparision over time more relevant than if the Region variable is used, because:

\begin{itemize}
\item The distances between measurement points in the same super clusters are smaller than the distances between measurement points in regions, ie, regions corresponds to larger areas.
\item Super clusters are guarranteed to be compatible between surveys, regions are only compatible between surveys for some countries.
\end{itemize}

To automatically partition each country into super clusters - areas which are suitable for repeated measurements analysis - we can start with a voronoi-diagram based on one survey in the country. The areas of that partition which lack measurements from other surveys can then iteratively be merged with neighbours which have such measurements until all areas have measurements from all surveyes. 

(Since some special DHS surveys cover only a small part of the country, we need a mechanism which allows us to exclude a certain survey from having influence on the partition, eg the ``6A'' survey in the Dominican Republic. As can be seen in figure, the two surveys ``52'' and ``61'' cover the whole country, while ``6A'' only covers three regions. If we would have let ``6A'' affect the partition, large areas would have been merged into clusters that are likely not reasonably similar.

Excluding some surveys can be useful for other reasons too, e.g. in a study of the effects of armed conflict we wanted to include only the 2008 and 2013 surveys for Nigeria since the 1990 didn't provide any data of use for us, and since including 1990 would have resulted in much larger areas.)

\section{External data sources}
\label{sec:extern-data-sourc}

The package is also able to automatically download and merge data from other sources than the DHS. Currently supported sources are: 
\begin{itemize}
\item The Quality of Government database
\item The malariaAtlas
\item Data on precipitation from urs.earthdata.nasa.gov
\item Data on temperature from berkeleyearth.lbl.gov
\end{itemize}

\section{Limitations}
\label{sec:limitations}

Version I of the DHS (surveys from 1986-1990) are not harmonised by the package.


\end{document}
