# Clean up the MICS_Core_Variables_to_harmonize.xlsx file (modified from original version for more effective clean-up in R) for later import back into R for variable harmonization

library("data.table")
library("rio")
library("stringi")
import::from(mgsub, mgsub)
import::from(janitor, remove_empty)
import::from(roperators, "%na<-%", "%regex<-%", "%ni%")


# import spreadsheet from the current directory
# If needed, change the working directory to the location of the file or either use the full file path
mics_crosswalk <- import("MICS_Core_Variables_to_harmonize.xlsx", sheet = "Cross Walk", skip = 3, setclass = "data.table", na = c("", "NA"))

# remove empty rows and columns
mics_crosswalk <- remove_empty(mics_crosswalk)


# remove single quotation marks/apostrophes from Variable label column
column_chose <- "Variable label"

# remove , (, ), and ' from Country names
for (col in column_chose) {
idx2 <- which(mics_crosswalk$"Variable label" %like% "`|'|‘|’")
set(mics_crosswalk, i = idx2, j = col, value = mgsub(mics_crosswalk[[col]][idx2], pattern = "`|'|‘|’", replacement = ""))
}

# save mics_crosswalk
save(mics_crosswalk, file = "MICS_Variables_Harmonize.RData")


# copy mics_crosswalk
mics_variables_harmonize <- copy(mics_crosswalk)




## Clean up the MICS Harmonization variables for later use in fully harmonizing the variable labels

## MICS 2

# capture the MICS 2 variable name column
mics2_variable_name <- mics_variables_harmonize$"Variable name on MICS2"

# split the string on ; and ,
mics2_variable_nameb <- strsplit(unlist(mics2_variable_name), ";|,")

# trim any white space from both the left & right
mics2_variable_namec <- stri_trim_both(unlist(mics2_variable_nameb))

# transform into a plain character vector
mics2_variable_named <- as.vector(mics2_variable_namec)

# get the location of the ; and , in the vector position
mics2_variable_name_location_add <- which(stri_detect_regex(mics2_variable_name, ",|;"))

# count the number of ; and ,
mics2_variable_name_location_count <- stri_count_regex(mics2_variable_name, ",|;")

# transform all NA_character_ values to 0
mics2_variable_name_location_count %na<-% 0

# determine which indices are not 0
# this will help us determine which variable label indices need to have add a NA_character_ at the selected index to make sure that the number of repeated rows are equivalent (in case they would not be otherwise)
mics2_variable_name_NOT_0 <- which(mics2_variable_name_location_count != 0)

# add 1 to the variable name location count
mics2_variable_name_location_count2 <- mics2_variable_name_location_count + 1


# capture the MICS 2 variable label column
mics2_variable_label <- mics_variables_harmonize$"Variable label on MICS2"

# split the string on ;
mics2_variable_labelb <- strsplit(unlist(mics2_variable_label), ";")

# trim any white space from both the left & right
mics2_variable_labelc <- stri_trim_both(unlist(mics2_variable_labelb))

# transform into a plain character vector
mics2_variable_labeld <- as.vector(mics2_variable_labelc)

# transform text NAs into true R character NAs
mics2_variable_labeld %regex<-% c("NA", NA_character_)

# get the location of the ; in the vector position
mics2_variable_label_location_add <- which(stri_detect_regex(mics2_variable_label, ";"))

# count the number of ;
mics2_variable_label_location_count <- stri_count_regex(mics2_variable_label, ";")

# transform all NA_character_ values to 0
mics2_variable_label_location_count %na<-% 0

# determine which indices are not 0
mics2_variable_label_NOT_0 <- which(mics2_variable_label_location_count != 0)

# add 1 to the variable label location count
mics2_variable_label_location_count2 <- mics2_variable_label_location_count + 1


# subset the original copied data.table with only the selected columns
mics2_variables_table <- mics_variables_harmonize[, .(ID, `Variable label`, `Location on MICS2`, `Variable name on MICS2`, `Variable label on MICS2`)]

# create a new column named Repeat that has the number of times to repeat each row
mics2_variables_table[, Repeat := mics2_variable_name_location_count2]

# repeat each row according to the count number in the Repeat column
# Source 1
# https://stackoverflow.com/questions/2894775/repeat-each-row-of-data-frame-the-number-of-times-specified-in-a-column | r - Repeat each row of data.frame the number of times specified in a column - Stack Overflow
mics2_variables_table <- mics2_variables_table[rep(seq(.N), Repeat), !"Repeat"]


if(length(mics2_variable_named) == length(mics2_variable_labeld)) {

# the indices where the NA_character_ needs to be added
mics2_add_NA_index <- mics2_variable_name_NOT_0[which(mics2_variable_name_NOT_0 %ni% mics2_variable_label_NOT_0)]

# Source 1 Begins
# https://stackoverflow.com/questions/1493969/how-to-insert-elements-into-a-vector

mics2_variable_labele <- c(mics2_variable_labeld, rep(NA_character_, length(mics2_add_NA_index)))

# "Each old element gets rank, each new element gets half-rank"
mics2_add_NA_index2 <- c(seq_along(mics2_variable_labeld), mics2_add_NA_index + 0.5)

# "Then use order to sort in proper order:"
mics2_variable_labelf <- mics2_variable_labele[order(mics2_add_NA_index2)]
# Source 1 Ends

# select the column name to modify
mics2_columnere1 <- "Variable name on MICS2"

# replace the existing contents of the selected column with the contents of mics2_variable_named (after the string split)
for (col in mics2_columnere1)
set(mics2_variables_table, j = col, value = mics2_variable_named)
# Source 5 end


# select the column name to modify
mics2_columnere2 <- "Variable label on MICS2"

# replace the existing contents of the selected column with the contents of mics2_variable_named (after the string split)
for (col in mics2_columnere2)
set(mics2_variables_table, j = col, value = mics2_variable_labelf)
# Source 5 end

} else {

# select the column name to modify
mics2_columnere1 <- "Variable name on MICS2"

# replace the existing contents of the selected column with the contents of mics2_variable_named (after the string split)
for (col in mics2_columnere1)
set(mics2_variables_table, j = col, value = mics2_variable_named)
# Source 5 end


# select the column name to modify
mics2_columnere2 <- "Variable label on MICS2"

# replace the existing contents of the selected column with the contents of mics2_variable_named (after the string split)
for (col in mics2_columnere2)
set(mics2_variables_table, j = col, value = mics2_variable_labeld)
# Source 5 end

}


# save mics2_variables_table
save(mics2_variables_table, file = "MICS2_Variables_table.RData")





## MICS 3

# capture the MICS 3 variable name column
mics3_variable_name <- mics_variables_harmonize$"Variable name on MICS3"

# split the string on ; and ,
mics3_variable_nameb <- strsplit(unlist(mics3_variable_name), ";|,")

# trim any white space from both the left & right
mics3_variable_namec <- stri_trim_both(unlist(mics3_variable_nameb))

# transform into a plain character vector
mics3_variable_named <- as.vector(mics3_variable_namec)

# get the location of the ; and , in the vector position
mics3_variable_name_location_add <- which(stri_detect_regex(mics3_variable_name, ",|;"))

# count the number of ; and ,
mics3_variable_name_location_count <- stri_count_regex(mics3_variable_name, ",|;")

# transform all NA_character_ values to 0
mics3_variable_name_location_count %na<-% 0

# determine which indices are not 0
# this will help us determine which variable label indices need to have add a NA_character_ at the selected index to make sure that the number of repeated rows are equivalent (in case they would not be otherwise)
mics3_variable_name_NOT_0 <- which(mics3_variable_name_location_count != 0)

# add 1 to the variable name location count
mics3_variable_name_location_count2 <- mics3_variable_name_location_count + 1


# capture the MICS 3 variable label column
mics3_variable_label <- mics_variables_harmonize$"Variable label on MICS3"

# split the string on ;
mics3_variable_labelb <- strsplit(unlist(mics3_variable_label), ";")

# trim any white space from both the left & right
mics3_variable_labelc <- stri_trim_both(unlist(mics3_variable_labelb))

# transform into a plain character vector
mics3_variable_labeld <- as.vector(mics3_variable_labelc)

# get the location of the ; in the vector position
mics3_variable_label_location_add <- which(stri_detect_regex(mics3_variable_label, ";"))

# count the number of ;
mics3_variable_label_location_count <- stri_count_regex(mics3_variable_label, ";")

# transform all NA_character_ values to 0
mics3_variable_label_location_count %na<-% 0

# determine which indices are not 0
mics3_variable_label_NOT_0 <- which(mics3_variable_label_location_count != 0)

# add 1 to the variable label location count
mics3_variable_label_location_count2 <- mics3_variable_label_location_count + 1


# subset the original copied data.table with only the selected columns
mics3_variables_table <- mics_variables_harmonize[, .(ID, `Variable label`, `Location on MICS3`, `Variable name on MICS3`, `Variable label on MICS3`)]

# create a new column named Repeat that has the number of times to repeat each row
mics3_variables_table[, Repeat := mics3_variable_name_location_count2]

# repeat each row according to the count number in the Repeat column
# Source 1
# https://stackoverflow.com/questions/2894775/repeat-each-row-of-data-frame-the-number-of-times-specified-in-a-column | r - Repeat each row of data.frame the number of times specified in a column - Stack Overflow
mics3_variables_table <- mics3_variables_table[rep(seq(.N), Repeat), !"Repeat"]


if(length(mics3_variable_named) == length(mics3_variable_labeld)) {

# the indices where the NA_character_ needs to be added
mics3_add_NA_index <- mics3_variable_name_NOT_0[which(mics3_variable_name_NOT_0 %ni% mics3_variable_label_NOT_0)]

# Source 1 Begins
# https://stackoverflow.com/questions/1493969/how-to-insert-elements-into-a-vector

mics3_variable_labele <- c(mics3_variable_labeld, rep(NA_character_, length(mics3_add_NA_index)))

# "Each old element gets rank, each new element gets half-rank"
mics3_add_NA_index2 <- c(seq_along(mics3_variable_labeld), mics3_add_NA_index + 0.5)

# "Then use order to sort in proper order:"
mics3_variable_labelf <- mics3_variable_labele[order(mics3_add_NA_index2)]
# Source 1 Ends

# select the column name to modify
mics3_columnere1 <- "Variable name on MICS3"

# replace the existing contents of the selected column with the contents of mics3_variable_named (after the string split)
for (col in mics3_columnere1)
set(mics3_variables_table, j = col, value = mics3_variable_named)
# Source 5 end


# select the column name to modify
mics3_columnere2 <- "Variable label on MICS3"

# replace the existing contents of the selected column with the contents of mics3_variable_named (after the string split)
for (col in mics3_columnere2)
set(mics3_variables_table, j = col, value = mics3_variable_labelf)
# Source 5 end

} else {

# select the column name to modify
mics3_columnere1 <- "Variable name on MICS3"

# replace the existing contents of the selected column with the contents of mics3_variable_named (after the string split)
for (col in mics3_columnere1)
set(mics3_variables_table, j = col, value = mics3_variable_named)
# Source 5 end


# select the column name to modify
mics3_columnere2 <- "Variable label on MICS3"

# replace the existing contents of the selected column with the contents of mics3_variable_named (after the string split)
for (col in mics3_columnere2)
set(mics3_variables_table, j = col, value = mics3_variable_labeld)
# Source 5 end

}


# save mics3_variables_table
save(mics3_variables_table, file = "MICS3_Variables_table.RData")





## MICS 4

# capture the MICS 4 variable name column
mics4_variable_name <- mics_variables_harmonize$"Variable name on MICS4"

# split the string on ; and ,
mics4_variable_nameb <- strsplit(unlist(mics4_variable_name), ";|,")

# trim any white space from both the left & right
mics4_variable_namec <- stri_trim_both(unlist(mics4_variable_nameb))

# transform into a plain character vector
mics4_variable_named <- as.vector(mics4_variable_namec)

# get the location of the ; and , in the vector position
mics4_variable_name_location_add <- which(stri_detect_regex(mics4_variable_name, ",|;"))

# count the number of ; and ,
mics4_variable_name_location_count <- stri_count_regex(mics4_variable_name, ",|;")

# transform all NA_character_ values to 0
mics4_variable_name_location_count %na<-% 0

# determine which indices are not 0
# this will help us determine which variable label indices need to have add a NA_character_ at the selected index to make sure that the number of repeated rows are equivalent (in case they would not be otherwise)
mics4_variable_name_NOT_0 <- which(mics4_variable_name_location_count != 0)

# add 1 to the variable name location count
mics4_variable_name_location_count2 <- mics4_variable_name_location_count + 1


# capture the MICS 4 variable label column
mics4_variable_label <- mics_variables_harmonize$"Variable label on MICS4"

# split the string on ;
mics4_variable_labelb <- strsplit(unlist(mics4_variable_label), ";")

# trim any white space from both the left & right
mics4_variable_labelc <- stri_trim_both(unlist(mics4_variable_labelb))

# transform into a plain character vector
mics4_variable_labeld <- as.vector(mics4_variable_labelc)

# transform text NAs into true R character NAs
mics4_variable_labeld %regex<-% c("NA", NA_character_)

# get the location of the ; in the vector position
mics4_variable_label_location_add <- which(stri_detect_regex(mics4_variable_label, ";"))

# count the number of ;
mics4_variable_label_location_count <- stri_count_regex(mics4_variable_label, ";")

# transform all NA_character_ values to 0
mics4_variable_label_location_count %na<-% 0

# determine which indices are not 0
mics4_variable_label_NOT_0 <- which(mics4_variable_label_location_count != 0)

# add 1 to the variable label location count
mics4_variable_label_location_count2 <- mics4_variable_label_location_count + 1


# subset the original copied data.table with only the selected columns
mics4_variables_table <- mics_variables_harmonize[, .(ID, `Variable label`, `Location on MICS4`, `Variable name on MICS4`, `Variable label on MICS4`)]

# create a new column named Repeat that has the number of times to repeat each row
mics4_variables_table[, Repeat := mics4_variable_name_location_count2]

# repeat each row according to the count number in the Repeat column
# Source 1
# https://stackoverflow.com/questions/2894775/repeat-each-row-of-data-frame-the-number-of-times-specified-in-a-column | r - Repeat each row of data.frame the number of times specified in a column - Stack Overflow
mics4_variables_table <- mics4_variables_table[rep(seq(.N), Repeat), !"Repeat"]


if(length(mics4_variable_named) != length(mics4_variable_labeld)) {

# the indices where the NA_character_ needs to be added
mics4_add_NA_index <- mics4_variable_name_NOT_0[which(mics4_variable_label_NOT_0 %ni% mics4_variable_name_NOT_0)]

# Source 1 Begins
# https://stackoverflow.com/questions/1493969/how-to-insert-elements-into-a-vector

mics4_variable_namee <- c(mics4_variable_named, rep(NA_character_, length(mics4_add_NA_index)))

# "Each old element gets rank, each new element gets half-rank"
mics4_add_NA_index2 <- c(seq_along(mics4_variable_named), mics4_add_NA_index + 0.5)

# "Then use order to sort in proper order:"
mics4_variable_namef <- mics4_variable_namee[order(mics4_add_NA_index2)]
# Source 1 Ends

# select the column name to modify
mics4_columnere1 <- "Variable name on MICS4"

# replace the existing contents of the selected column with the contents of mics4_variable_named (after the string split)
for (col in mics4_columnere1)
set(mics4_variables_table, j = col, value = mics4_variable_namef)
# Source 5 end


# select the column name to modify
mics4_columnere2 <- "Variable label on MICS4"

# replace the existing contents of the selected column with the contents of mics4_variable_named (after the string split)
for (col in mics4_columnere2)
set(mics4_variables_table, j = col, value = mics4_variable_labeld)
# Source 5 end

} else {

# select the column name to modify
mics4_columnere1 <- "Variable name on MICS4"

# replace the existing contents of the selected column with the contents of mics4_variable_named (after the string split)
for (col in mics4_columnere1)
set(mics4_variables_table, j = col, value = mics4_variable_named)
# Source 5 end


# select the column name to modify
mics4_columnere2 <- "Variable label on MICS4"

# replace the existing contents of the selected column with the contents of mics4_variable_named (after the string split)
for (col in mics4_columnere2)
set(mics4_variables_table, j = col, value = mics4_variable_labeld)
# Source 5 end

}


# save mics4_variables_table
save(mics4_variables_table, file = "MICS4_Variables_table.RData")






## MICS 5

# capture the MICS 5 variable name column
mics5_variable_name <- mics_variables_harmonize$"Variable name on MICS5"

# split the string on ; and ,
mics5_variable_nameb <- strsplit(unlist(mics5_variable_name), ";|,")

# trim any white space from both the left & right
mics5_variable_namec <- stri_trim_both(unlist(mics5_variable_nameb))

# transform into a plain character vector
mics5_variable_named <- as.vector(mics5_variable_namec)

# get the location of the ; and , in the vector position
mics5_variable_name_location_add <- which(stri_detect_regex(mics5_variable_name, ",|;"))

# count the number of ; and ,
mics5_variable_name_location_count <- stri_count_regex(mics5_variable_name, ",|;")

# transform all NA_character_ values to 0
mics5_variable_name_location_count %na<-% 0

# determine which indices are not 0
# this will help us determine which variable label indices need to have add a NA_character_ at the selected index to make sure that the number of repeated rows are equivalent (in case they would not be otherwise)
mics5_variable_name_NOT_0 <- which(mics5_variable_name_location_count != 0)

# add 1 to the variable name location count
mics5_variable_name_location_count2 <- mics5_variable_name_location_count + 1


# capture the MICS 5 variable label column
mics5_variable_label <- mics_variables_harmonize$"Variable label on MICS5"

# split the string on ;
mics5_variable_labelb <- strsplit(unlist(mics5_variable_label), ";")

# trim any white space from both the left & right
mics5_variable_labelc <- stri_trim_both(unlist(mics5_variable_labelb))

# transform into a plain character vector
mics5_variable_labeld <- as.vector(mics5_variable_labelc)

# transform text NAs into true R character NAs
mics5_variable_labeld %regex<-% c("NA", NA_character_)

# get the location of the ; in the vector position
mics5_variable_label_location_add <- which(stri_detect_regex(mics5_variable_label, ";"))

# count the number of ;
mics5_variable_label_location_count <- stri_count_regex(mics5_variable_label, ";")

# transform all NA_character_ values to 0
mics5_variable_label_location_count %na<-% 0

# determine which indices are not 0
mics5_variable_label_NOT_0 <- which(mics5_variable_label_location_count != 0)

# add 1 to the variable label location count
mics5_variable_label_location_count2 <- mics5_variable_label_location_count + 1


# subset the original copied data.table with only the selected columns
mics5_variables_table <- mics_variables_harmonize[, .(ID, `Variable label`, `Location on MICS5`, `Variable name on MICS5`, `Variable label on MICS5`)]

# create a new column named Repeat that has the number of times to repeat each row
mics5_variables_table[, Repeat := mics5_variable_name_location_count2]

# repeat each row according to the count number in the Repeat column
# Source 1
# https://stackoverflow.com/questions/2894775/repeat-each-row-of-data-frame-the-number-of-times-specified-in-a-column | r - Repeat each row of data.frame the number of times specified in a column - Stack Overflow
mics5_variables_table <- mics5_variables_table[rep(seq(.N), Repeat), !"Repeat"]


if(length(mics5_variable_named) != length(mics5_variable_labeld)) {

# the indices where the NA_character_ needs to be added
mics5_add_NA_index <- mics5_variable_name_NOT_0[which(mics5_variable_label_NOT_0 %ni% mics5_variable_name_NOT_0)]

# Source 1 Begins
# https://stackoverflow.com/questions/1493969/how-to-insert-elements-into-a-vector

mics5_variable_namee <- c(mics5_variable_named, rep(NA_character_, length(mics5_add_NA_index)))

# "Each old element gets rank, each new element gets half-rank"
mics5_add_NA_index2 <- c(seq_along(mics5_variable_named), mics5_add_NA_index + 0.5)

# "Then use order to sort in proper order:"
mics5_variable_namef <- mics5_variable_namee[order(mics5_add_NA_index2)]
# Source 1 Ends

# select the column name to modify
mics5_columnere1 <- "Variable name on MICS5"

# replace the existing contents of the selected column with the contents of mics5_variable_named (after the string split)
for (col in mics5_columnere1)
set(mics5_variables_table, j = col, value = mics5_variable_namef)
# Source 5 end


# select the column name to modify
mics5_columnere2 <- "Variable label on MICS5"

# replace the existing contents of the selected column with the contents of mics5_variable_named (after the string split)
for (col in mics5_columnere2)
set(mics5_variables_table, j = col, value = mics5_variable_labeld)
# Source 5 end

} else {

# select the column name to modify
mics5_columnere1 <- "Variable name on MICS5"

# replace the existing contents of the selected column with the contents of mics5_variable_named (after the string split)
for (col in mics5_columnere1)
set(mics5_variables_table, j = col, value = mics5_variable_named)
# Source 5 end


# select the column name to modify
mics5_columnere2 <- "Variable label on MICS5"

# replace the existing contents of the selected column with the contents of mics5_variable_named (after the string split)
for (col in mics5_columnere2)
set(mics5_variables_table, j = col, value = mics5_variable_labeld)
# Source 5 end

}


# save mics5_variables_table
save(mics5_variables_table, file = "MICS5_Variables_table.RData")





## MICS 6

# capture the MICS 6 variable name column
mics6_variable_name <- mics_variables_harmonize$"Variable name on MICS6"

# split the string on ; and ,
mics6_variable_nameb <- strsplit(unlist(mics6_variable_name), ";|,")

# trim any white space from both the left & right
mics6_variable_namec <- stri_trim_both(unlist(mics6_variable_nameb))

# transform into a plain character vector
mics6_variable_named <- as.vector(mics6_variable_namec)

# get the location of the ; and , in the vector position
mics6_variable_name_location_add <- which(stri_detect_regex(mics6_variable_name, ",|;"))

# count the number of ; and ,
mics6_variable_name_location_count <- stri_count_regex(mics6_variable_name, ",|;")

# transform all NA_character_ values to 0
mics6_variable_name_location_count %na<-% 0

# determine which indices are not 0
# this will help us determine which variable label indices need to have add a NA_character_ at the selected index to make sure that the number of repeated rows are equivalent (in case they would not be otherwise)
mics6_variable_name_NOT_0 <- which(mics6_variable_name_location_count != 0)

# add 1 to the variable name location count
mics6_variable_name_location_count2 <- mics6_variable_name_location_count + 1


# capture the MICS 6 variable label column
mics6_variable_label <- mics_variables_harmonize$"Variable label on MICS6"

# split the string on ;
mics6_variable_labelb <- strsplit(unlist(mics6_variable_label), ";")

# trim any white space from both the left & right
mics6_variable_labelc <- stri_trim_both(unlist(mics6_variable_labelb))

# transform into a plain character vector
mics6_variable_labeld <- as.vector(mics6_variable_labelc)

# transform text NAs into true R character NAs
mics6_variable_labeld %regex<-% c("NA", NA_character_)

# get the location of the ; in the vector position
mics6_variable_label_location_add <- which(stri_detect_regex(mics6_variable_label, ";"))

# count the number of ;
mics6_variable_label_location_count <- stri_count_regex(mics6_variable_label, ";")

# transform all NA_character_ values to 0
mics6_variable_label_location_count %na<-% 0

# determine which indices are not 0
mics6_variable_label_NOT_0 <- which(mics6_variable_label_location_count != 0)

# add 1 to the variable label location count
mics6_variable_label_location_count2 <- mics6_variable_label_location_count + 1

# the indices where the NA_character_ needs to be added
mics6_add_NA_index <- mics6_variable_name_NOT_0[which(mics6_variable_name_NOT_0 %ni% mics6_variable_label_NOT_0)]

# Source 1 Begins
# https://stackoverflow.com/questions/1493969/how-to-insert-elements-into-a-vector

mics6_variable_labele <- c(mics6_variable_labeld, rep(NA_character_, length(mics6_add_NA_index)))

# "Each old element gets rank, each new element gets half-rank"
mics6_add_NA_index2 <- c(seq_along(mics6_variable_labeld), mics6_add_NA_index + 0.5)

# "Then use order to sort in proper order:"
mics6_variable_labelf <- mics6_variable_labele[order(mics6_add_NA_index2)]

# Source 1 Ends



# subset the original copied data.table with only the selected columns
mics6_variables_table <- mics_variables_harmonize[, .(ID, `Variable label`, `Location on MICS6`, `Variable name on MICS6`, `Variable label on MICS6`)]

# create a new column named Repeat that has the number of times to repeat each row
mics6_variables_table[, Repeat := mics6_variable_name_location_count2]

# repeat each row according to the count number in the Repeat column
# Source 1
# https://stackoverflow.com/questions/2894775/repeat-each-row-of-data-frame-the-number-of-times-specified-in-a-column | r - Repeat each row of data.frame the number of times specified in a column - Stack Overflow
mics6_variables_table <- mics6_variables_table[rep(seq(.N), Repeat), !"Repeat"]

# select the column name to modify
mics6_columnere1 <- "Variable name on MICS6"

# replace the existing contents of the selected column with the contents of mics6_variable_named (after the string split)
for (col in mics6_columnere1)
set(mics6_variables_table, j = col, value = mics6_variable_named)
# Source 5 end


# select the column name to modify
mics6_columnere2 <- "Variable label on MICS6"

# replace the existing contents of the selected column with the contents of mics6_variable_named (after the string split)
for (col in mics6_columnere2)
set(mics6_variables_table, j = col, value = mics6_variable_labelf)
# Source 5 end


# save mics6_variables_table
save(mics6_variables_table, file = "MICS6_Variables_table.RData")
