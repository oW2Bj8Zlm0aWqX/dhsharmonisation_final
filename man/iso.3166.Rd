\docType{data}                                                                                                    
\name{iso.3166}                                                                                           
\alias{iso.3166}                                                                                          
\title{The ISO 3166-alpha-3 standard for names and code for countries}
\format{A data frame with three variables and 241 observations}
\source{                                                                                                          
  United Nations.
}                                                                                                                 
\description{
  An integer code, a string and a three-letter code for each country.
}                                                                                                                 
\author{                                                                                                          
  Hans Ekbrand
}                                                                                                                 
\usage{iso.3166}                                                                                          
