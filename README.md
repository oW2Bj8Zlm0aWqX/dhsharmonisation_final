# README #

## What is this repository for? ##

With this R package, I want to help researchers by providing harmonised surveydata useful for analysis of poverty, development, and 
more generally, global living conditions.

1.0 Release date 2019-02-22

## Introduction

I'm in a hurry, what are the instructions for a minimal data set?

    install.packages("devtools")
    library(devtools)
    install_bitbucket(repo = "hansekbrand/DHSharmonisation")
    library(globallivingconditions)
    dt <- download.and.harmonise(dhs.user="", <-- your username at DHS goes here
                           dhs.password="", <-- your password at DHS goes here
                           log.filename="living-conditions.log")

`dt` will now have a minimal data set without external sources. Note that if you have access to many countries `download.and.harmonise()` will take many hours (Even a single country can take an hour).

Read on if you need more than that.

## How to install the package "globallivingconditions" ##

Until this package is available on CRAN, I recommend install the package with `devtools`:

  A.    Install `devtools` if you do not already have it installed.

Note that if your OS *compiles* R packages (rather than copying ready-made binaries from the internet), i.e. if you are on OS X or Linux, then to build `devtools`, development files for `curl` are necessary, and supplied in Debian GNU/Linux by the package `libcurl4-gnutls-dev`. `devtools` also depends on `openssl` which requires development files included in `libssl-dev`. `globallivingconditions` depends on `XML` which has `libxml2-dev` as a build dependency, and `rgdal` which in turn depends on `libgdal-dev` and `libproj-dev`. For Debian GNU/Linux the following command will make sure that the dependencies are satisfied:

    apt-get install libcurl4-gnutls-dev libssl-dev libxml2-dev libgdal-dev libproj-dev 

Issue this within R

    install.packages("devtools")
    library(devtools)

  B.    Use `devtools` to install "globallivingconditions"

Issue this within R

    install_bitbucket(repo = "hansekbrand/DHSharmonisation")

This will automatically install all dependencies, recursively.

## Deployment instructions ##

I have found it convenient to have my credentials stored in a file which can be loaded by many different scripts. It also makes it possible to share scripts with others without sharing your secret credential.

    credentials <- list(dhs.user = "foo@bar.com", dhs.password = "r3al.password")
    save(credentials, file = paste0(normalizePath("~"), "user_credentials_dhs.RData", sep = "/"))
    
This way, the credentials can be loaded regardless of the current directory

    library(globallivingconditions)
    load("user_credentials_dhs.RData") # a list with two named elements:
                                       # "dhs.user" and "dhs.password" 
    my.dt <- download.and.harmonise(
        dhs.user=credentials$dhs.user,
        dhs.password=credentials$dhs.password,
        iwi.path = paste0(my.data.dir, "iwi"),
        drought.data.path = paste0(my.data.dir, "vcpct--VIC_DERIVED"),                                
        flooding.data.path = paste0(my.data.dir, "runoff--VIC_3B42RT"),
        armed.conflict.filename = paste0(
            my.data.dir,
            "Armed-Conflict/intensity-of-armed-conflicts-Africa-UCDP.RData"),
        countries = c("Nigeria"),
        log.filename = "living-conditions.log"
    )

To get a minimal data set, without any external data sources, simply exclude the path arguments, since they default to NULL.

    library(globallivingconditions)
    load("user_credentials_dhs.RData") # a list with two named elements:
                                       # "dhs.user" and "dhs.password" 
    download.and.harmonise(dhs.user=credentials$dhs.user,
                           dhs.password=credentials$dhs.password,
                           log.filename="living-conditions.log")



## FAQ ##

### Why is this package written in form of a knitr-document? ###

The end user will not notice that knitr is involved under the hood,
the end user will only see a function that given username and password
to the DHS-server, will return the dataset.

Perhaps we won't need knitr when we are done, but
during development, I think it is a good choice, for three reasons:

#### 1. The ability to cache work done during development. ####

We are dealing with 23 GB of data which, in many steps is refined into
a dataset of about 11.5 million rows and 130 MB of data. For clarity
this process is divided into blocks that is small enough to
comprehend. The most computationally efficient way would be to only
load one piece of data once, and do all manipulations on it right
away. However, for a human to grasp all the manipulations,
partitioning the task into small bits is much easier to understand,
modify, and debug.

knitr automates parts of the caching needed for that workflow. The low
RAM requirement (which came later in the process, long after I had
settled on knitr), reduced the strength of the caching part somewhat,
since I use external files for more and more of the caching.


#### 2. To minimise the work required to keep documentation and code in sync ####

To minimise the work required to keep documentation and code in sync,
ie for the same reason that knitr, or, more generally, literate
programming (before there was knitr I used pgfSweave), is a good
choice for replicable research.


#### 3. The output of that document is useful for validation ####

Also, the harmonisation program is rather big, about 2200 lines of
code. If instead of knitr-document I would have made one master
function that controlled the complete process, then I would have to
explain what it did, and the kind of objects it deals with and
creates, by means of comments. With knitr, I can easily show the data
by creating a table or a graph. All tables and graphs in the PDF are
strictly speaking not necessary to achieve the harmonisation, but they
do a good job explaining what is happening, and they are essential to
validation - we can immediatly tell that this categorisation of a type
of floor is correctly or incorrectly classified.

## Who do I talk to? ##

> Hans Ekbrand <hans.ekbrand@gu.se>
> University of Gothenburg, department of sociology and work science
> Sweden