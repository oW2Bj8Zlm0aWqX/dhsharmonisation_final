country.specific.ir.and.mr.f <- function(pair, vars.ir.file, 
                                         vars.mr.file){
    my.log.f(paste(Sys.time(), "country.specific.ir.and.mr.f, ir-file:", 
                pair$i.files, "mr-file:", pair$m.files))
    all.vars.ir <- c("HouseholdID", "PersonID.unique", vars.ir.file)
    if(is.na(pair$i.files) == FALSE){
        mothers.full <- get(load(paste0(pair$directory, pair$i.files)))
        my.error <- try(mothers <- mothers.full[, ..all.vars.ir])
        if(length(attr(my.error, which = "condition")) > 0){
   my.log.f(paste(Sys.time(), "country.specific.ir.and.mr.f() failed",
       "with this error on file", paste0(pair$directory, pair$i.files),
       attr(my.error, which = "condition")))
        }
        ## remove empty columns
        for(var in names(mothers)){
            if(length(which(is.na(mothers[[var]]))) == nrow(mothers)){
                mothers[[var]] <- NULL
            }
        }
    }
    fathers.file <- pair$m.files
    if(is.na(fathers.file) == FALSE){    
        all.vars.mr <- c("HouseholdID", "PersonID.unique", vars.mr.file)
        fathers.full <- get(load(paste0(pair$directory, pair$m.files)))
        fathers <- fathers.full[, ..all.vars.mr]
        ## remove empty columns
        for(var in names(fathers)){
            if(length(which(is.na(fathers[[var]]))) == nrow(fathers)){
                fathers[[var]] <- NULL
            }
        }
    }
    if(exists("mothers") & exists("fathers")){
    ## Due to errors in the data, mostly in Peru DHS-III, some fathers have the 
    ## same ID as an existing mother, in which case we remove the father since
    ## there is more data on the mother. (perhaps Peru DHS-III need another
    ## Unique household identifier?
        remove.these <- which(fathers$PersonID.unique %in% 
                              mothers$PersonID.unique)
        if(length(remove.these) > 0){
            fathers <- fathers[-remove.these]
        }
        ir.and.mr <- rbind(mothers, fathers, fill=TRUE)
    }
    if(exists("mothers") & ! exists("fathers")){
        ir.and.mr <- mothers
    }
    if(!exists("mothers") & exists("fathers")){
        ## not likely to exist, but for completeness
        ir.and.mr <- fathers
    }
    
    ## Nota Bene this slice must be merged properly, since its order and 
    ## members are not the same as the PR file(s) (or the merged.slim.files 
    ## if KR files are available)
    
    ## Since we now have one part already in RAM, load the other and do the 
    ## merge now.
    
    ## if the merged file exists then use that, otherwise use the file
    version <- substring(pair$p.files, 5, 6)
    merged.filename <- paste0(version, ".merged.slim.dt.RData")
    if(file.exists(paste0(pair$directory, merged.filename))){
        slim.file <- paste0(pair$directory, merged.filename)
    } else {
        slim.file <- paste0(pair$directory, paste(gsub("RData", "slim.dt",
                                        pair$p.files), "RData", sep = "."))
    }
    if(file.exists(slim.file)){
        load(slim.file) ## my.dt
    } else {
        my.log.f(paste(Sys.time(), "country.specific.ir.and.mr.data found no",
                       "merged file", merged.filename,
         "nor any slim.dt", slim.file, "in directory", pair$directory,
         "which would happen if the user specified",
          "KR files but DHS does not provide KR files for this particular survey.",
       "Long term solution: make my.add.16.to.kr.and.missing.children.to.pr()",
          "work even if there is no KR file to merge. This is now implemented",
            "so this is an unexpected error"))
        return(NULL)
    }
    
    ## in case both these particular IR and MR files are missing, the object 
    ## ir.and.mr is missing, then only rename the file:
    ## e.g. "61.merged.slim.dt.RData" -> "61.merged.slim.dt.core.RData"
    if(!exists("ir.and.mr")){
        new.filename <- gsub(".RData", ".core.RData", slim.file)
        my.log.f(paste(Sys.time(), "found no MR or IR file",
          pair$directory, "so I'll simply copy the file", slim.file,
          "to", new.filename))
        file.copy(from=slim.file, to=new.filename)
        return(NULL)
    }

    ir.and.mr[, HouseholdID := NULL]
    
    ## Store the original order
    ## ENFORCE A FULL COPY
    ## my.PersonID.unique <- my.dt$PersonID.unique ## BUG
    my.PersonID.unique <- data.table::copy(my.dt$PersonID.unique) ## CORRECT
    setkey(my.dt, PersonID.unique)
    setkey(ir.and.mr, PersonID.unique)
    my.dt.new <- merge(my.dt, ir.and.mr, all.x=TRUE)
    ## Restore the original order
    my.dt.newer <- my.dt.new[match(my.PersonID.unique, my.dt.new$PersonID.unique), ]
    
    save(my.dt.newer, file = gsub(".RData", ".core.RData", slim.file))
}

