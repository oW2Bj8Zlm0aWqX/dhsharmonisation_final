make.slim.files <- function(file, variable.packages, temp.pairs.df, file.types.to.download){
    new.filename <- gsub("RData", "slim.dt", file)
    ## Create a lock file that we remove on error-free exit.
    lock.filename <- paste0(new.filename, ".lock")
    my.log.f(paste(Sys.time(), "converting file:", file, "to", new.filename,
                   "using lock file", lock.filename))
    writeChar(object="a", con=lock.filename)
    ## To fit the process within 8 GB of RAM, we need to first create a thin
    ## data.table object (with only a few variable), and later on we can add
    ## more variables.
    
    ## Note that my.dt.full is country specific and thus relatively small.
    
    ## First create a full object
    my.dt.full <- get(load(file))

    ## Save space, use factors instead of character vectors
    ## But keep "ClusterID.clean" as numeric for later matching
    ## with coordinates.RData
    for(var in c("source", "version", "RegionID", "ClusterID",
                 "HouseholdID")){
        if(length(grep(var, names(my.dt.full))) > 0) {
            my.dt.full[[var]] <- factor(my.dt.full[[var]])
        }
    }
    ## Then create thin objects that we save to disk and only later
    ## will merge in.
    ## iwi is now optional
    candidate.vars.for.living.cond <- 
    c("iwi", "has.electricity", "rural", "phone",
                                  "has.computer", "has.video", "owns.radio",
                                  "owns.tv", "wall", "roof", "native.language",
                 "has.fan", "has.table", "has.oven", "information.deprivation",
                                  "education.in.years", "education", "caste",
                                  "severe.education.deprivation")
    vars.for.living.cond <- candidate.vars.for.living.cond[
        candidate.vars.for.living.cond %in% colnames(my.dt.full)]
    living.cond <- my.dt.full[, vars.for.living.cond, with = FALSE]
    save(living.cond, file = paste0(new.filename, ".living.cond.RData"))
    rm(living.cond)    
    
    ## Be robust, these variables might be all NA
    misc.goods <- my.dt.full[, c("shares.toilet", "type.of.cooking.fuel",
          "place.to.wash.hands", "person.who.fetches.water", "water.made.safe",
          "cooking.on.stove.or.open.fire", "refridgerator", "bicycle",
          "motorcycle", "car", "animal.drawn.cart", "boat.with.motor",
          "owns.land", "hectares.of.land", "owns.livestock", "has.bankaccount",
          "nr.of.sleeping.rooms", "water", "time.to.water"),
                     with=FALSE]
    save(misc.goods, file = paste0(new.filename, ".misc.goods.RData"))
    rm(misc.goods)
    
    my.dt.full[, sex := car::recode(sex, recodes = "
     c('female', 'Female transgender') = 'Female'; 
     c('male', 'Male transgender') = 'Male'"), ]


    if(length(grep("KR", file.types.to.download)) > 0){
        health <- my.dt.full[, c("did.not.receive.treatment", "no.immunization", 
                 "no.polio.vacc", "no.dpt.vacc", "no.vacc.measles"), with=FALSE]
        save(health, file = paste0(new.filename, ".health.RData"))
        rm(health)
    }

    ## Change: do this even if IR files are not available. This is now the only
    ## way to get vital variables as HouseholdID and PersonID into the final file
    
    # The maternal-links files are not available in temp.pairs.df

    ## If there is a maternal-links file in the same path as our main file,
    ## then use it to improve the data on parenthood before saving.
    
    ## Some directories contain multiple surveys, (TODO: check if this still holds) 
    ## so derive the filename and test for existance. From the file name of
    ## the merged file, e.g. "DHS-V/Dominican Republic 2007/52.merged.RData"
    ## construct the PR-filename
    
    my.path <- sapply(strsplit(file, "/"), 
                              function(x) { paste(x[1], x[2], sep = "/") })
    my.versions <- substring(temp.pairs.df$p.files[grep(my.path, 
                                            temp.pairs.df$directory)], 5, 6)
    my.ir.files <- substring(temp.pairs.df$i.files[grep(my.path, 
                                            temp.pairs.df$directory)], 1, 15)
    my.real.version <- substring(strsplit(file, "/")[[1]][3], 1, 2)

    this.one <- match(my.real.version, my.versions)
    maternal.links.file <- paste0(my.path, "/", my.ir.files[this.one], 
                                  "maternal-links")
    if(!file.exists(maternal.links.file)){ 
        maternal.links.file <- NULL 
    }
    
    line.number <- add.line.number.parent.outer(my.dt.full[, c(
     "PersonID.unique", "PersonID", "sex", "line.number.mother",
     "line.number.father", "HouseholdID", "relation.to.hh")], 
     maternal.links.file)
    ## reorder to fit the original order
    my.order <- match(my.dt.full$PersonID.unique, line.number$PersonID.unique)
    line.number <- line.number[my.order, ]

    ## delete PersonID.unique
    line.number[, c("PersonID.unique") := NULL]
    save(line.number, file = paste0(new.filename, ".line.number.RData"))
    my.log.f(paste(Sys.time(), "linking parents in", paste0(new.filename, 
                           ".line.number.RData")))
    rm(line.number)

    ## age did not fit in elsewhere.

    meta.data <- my.dt.full[, c("age", "month.of.interview",
                                "sample.weight", "HHID"), with=FALSE]
    save(meta.data, file=paste0(new.filename, ".meta.data.RData"))
    rm(meta.data)
    
    ## Support variable.packages, for each package, save all vars in a file.
    ## do not assume all variables exists at this point (we might have already 
    ## compounded raw data to derived variables, e.g. the various phone vars)

    my.error <- try(
    if(is.null(variable.packages) == FALSE){
        ## my.log.f(paste(Sys.time(), "variable.packages is not NULL"))
        for(package in variable.packages){
            ## Gather a vector of potential variable names, from all file types
           vars <- paste0(package, ".", as.vector(unlist(sapply(file.types.to.download,
                   variable.packages.names.f, variable.packages = package,
                   name.type = "New.variable.name"))))
            my.log.f(paste(Sys.time(), "found variable names", 
  paste(vars, collapse = ", "), "for package", package))
            if(length(na.omit(match(vars, names(my.dt.full)))) > 0){
                data <- try(my.dt.full[, na.omit(match(vars, names(my.dt.full))), with=FALSE], silent=FALSE)
                if(length(attr(data, which = "condition")) > 0){
                    my.log.f(paste(Sys.time(), "failed to find data for variable.packages for file", file,
                                   "error given", data))        
                }
                
        save(data, file = paste0(new.filename, ".", package, ".package.RData"))
                my.log.f(paste(Sys.time(), "saving data for variable.package:",
                               package, "file", file))
            } else {
            my.log.f(paste(Sys.time(), "no data to save for variable.package:",
             package, "in file", file, "tried these variables: ", 
             paste(vars, collapse = ", ")))
            }
        }
    } else {
        ## my.log.f(paste(Sys.time(), "variable.packages is NULL"))        
    }
, silent=FALSE)
    if(length(attr(my.error, which = "condition")) > 0){
        my.log.f(paste(Sys.time(), "failed at variable.packages for file", file,
                       "error given: ", attr(my.error, which = "condition")))
    }

    ## Then create a stripped down copy with only the essentials.
    ## This object will be saved and later merged with data from ir and mr.

    ## magic bug: the chunk only works when evalued in try()
    ## my.error <- try(
    my.dt <- my.dt.full[, c("source", "version", "year.of.interview", 
             "country.code.ISO.3166.alpha.3", "m49.region", "RegionID", "district", 
             "ClusterID", "ClusterID.clean", "PersonID.unique"), with=FALSE]
    ## )
    ## if(length(attr(my.error, which = "condition")) > 0){
    ##     my.log.f(paste(Sys.time(), "failed at creating file", paste0(new.filename, ".RData"),
    ##                    "error given: ", attr(my.error, which = "condition")))
    ## }
    ## my.error <- try(
    save(my.dt, file = paste0(new.filename, ".RData"))
    ## )
    ## if(length(attr(my.error, which = "condition")) > 0){
    ##     my.log.f(paste(Sys.time(), "failed at saving file", paste0(new.filename, ".RData"),
    ##                    "error given: ", attr(my.error, which = "condition")))
    ## }
    
    my.log.f(paste(Sys.time(), "Finished making slim files from", file,
                   "removing the lock file", lock.filename))
    unlink(lock.filename)
}
